# README #

This is a version of the LTKPerl RFID::LLRP module that has current xml definitions and is set up to install.

### What is this repository for? ###

Implementing perl programs that use LLRP to access RFID readers, e.g. Impinj R420, R220 or R1000.


### How do I get set up? ###

* perl Makefile.pl
* make install